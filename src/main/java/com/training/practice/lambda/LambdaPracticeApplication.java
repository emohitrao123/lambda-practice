package com.training.practice.lambda;

import com.training.practice.service.LambdaScenarioForString;
import com.training.practice.service.LambdaScenarios;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.swing.plaf.synth.SynthEditorPaneUI;

@SpringBootApplication
public class LambdaPracticeApplication {

	public static void main(String[] args) {

		SpringApplication.run(LambdaPracticeApplication.class, args);

		/*LambdaScenarios geninterf = new LambdaScenariosImpl();
		geninterf.helloWorld();*/

		/*LambdaScenarios lambdainterf =   () -> System.out.println("Hello World by Lambda Expression");
		lambdainterf.helloWorld();*/

		LambdaScenarios lambdainterf =  (int a, int b) -> System.out.println("Sum of two number via Lambda Expression is "+ (a + b));
		lambdainterf.sum(14,15);
		lambdainterf.sum(200,300);

		LambdaScenarioForString lambdapart2 =   str-> str.length();
		System.out.println("Length of Mohit's name "+lambdapart2.getLength("Mohit"));
		System.out.println("Length of Alex's name "+lambdapart2.getLength("Alex"));
		System.out.println("Length of Lubna's name "+lambdapart2.getLength("Lubna"));


	}


}
