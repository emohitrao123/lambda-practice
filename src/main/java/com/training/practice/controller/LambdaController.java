package com.training.practice.controller;

import com.training.practice.service.LambdaScenarioForString;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;

@RestController
public class LambdaController {

    LambdaScenarioForString lambdapart2 = str-> str.length();
    @RequestMapping(method = RequestMethod.GET, path = "/name/{len}")
    //@GetMapping("/name/{lname}")
    public int getLength(@PathVariable String len){

        return lambdapart2.getLength(len);

    }

}
