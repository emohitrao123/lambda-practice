package com.training.practice.service;

public interface LambdaScenarioForString {

    public int getLength(String str);

}
